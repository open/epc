<?php
namespace RongYao\Epc;

interface OcrInterface
{
    public function carLicense(string $code, string $side);

    public function regCert(string $code);

    public function idCard(string $code, string $side);

    public function businessLicense(string $code);

    public function vin(string $code);

}