<?php
namespace RongYao\Epc;

interface AssemblyInterface
{
    public function match(array $items, string $epcModelId);

    public function getAllParts(string $epcModelId, string $vin);

    public function getAssemblies(string $epcModelId, string $vin);
}