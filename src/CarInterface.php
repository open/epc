<?php
namespace RongYao\Epc;

interface CarInterface
{
    public function getBaseInfo(string $vin);

    public function getVin(string $base64);

    public function getBrands(string $keywords);
    
    public function getSeries(string $epcMainBrandId, ?string $keywords = null);

    public function getModels(string $epcSeriesId, ?string $keywords = null);

    public function getModel(string $epcModelId);

    public function getModelDetail(string $epcModelId);

    public function getGroupBrands();

    public function getGroupSeries(string $epcMainBrandId);

    public function getGroupModels(string $epcSeriesId);

    public function excludeAutoGearBox(string $epcModelId);

    public function excludeSlidingDoor(string $epcModelId);
}