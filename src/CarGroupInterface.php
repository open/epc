<?php
namespace RongYao\Epc;

interface CarGroupInterface
{
    public function getMajorGroup(string $epcModelId);

    public function getLeafGroupIds(string $epcModelId);

    public function getGroupTree(string $epcModelId, string $jyGroupId);

    public function getParts(string $epcModelId, string $jyGroupId);

    public function getPic(string $epcModelId, string $jyGroupId);

    public function getAllParts(string $epcModelId, string $vin = '');
}