<?php
namespace RongYao\Epc;

interface OeInterface
{
    public function oeSearch(array $oes);

    public function oeFuzzySearch(string $epcModelId, array $oes, string $vin = '');

    public function nameFuzzySearch(string $epcModelId, array $names, string $vin = '');

    public function getPrice(string $epcOeId);

    public function getDetail(string $epcOeId);

    public function getPic(string $epcModelId,string $epcOeId, string $jyGroupId);

    public function getApplicableModels(string $epcBrandId, string $oe);

    public function getApplicableSeries(string $epcBrandId, string $oe);
}