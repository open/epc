<?php
namespace RongYao\Epc;

interface CarPositionInterface
{
    public function getParts(string $epcModelId, string $collisionPosition);
}